# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'resque/single/version'

Gem::Specification.new do |spec|
  spec.name          = "resque-single"
  spec.version       = Resque::Single::VERSION
  spec.authors       = ["bibendi"]
  spec.email         = ["bibendi@bk.ru"]
  spec.summary       = 'Integration of resque with resque-progress and resque-lock'
  spec.homepage      = "https://bitbucket.org/shiplix/resque-single"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'activesupport', '>= 3.0'
  spec.add_runtime_dependency 'resque', '~> 1.25'
  spec.add_runtime_dependency 'resque-lock', '~> 1.1.0'
  spec.add_runtime_dependency 'resque-meta', '>= 2.0.0'
  spec.add_runtime_dependency 'resque-progress', '~> 1.0.1'

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
